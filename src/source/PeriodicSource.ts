import {Sink, Source, Scheduler, Disposable} from '../index';
import {Stream} from '../Stream';
import {create} from '../disposable/dispose';
import {PropagateTask} from '../scheduler/PropagateTask';

function emit(t, x, sink) {
  sink.event(t, x);
}

function cancelTask(task) {
  task.cancel();
}

class PeriodicSource<T> implements Source<T> {
  constructor(private period: number, private value: T) {}

  run(sink: Sink<T>, scheduler: Scheduler): Disposable<any> {
    const task = scheduler.periodic(this.period, new PropagateTask(emit, this.value, sink));
    return create(cancelTask, task);
  }
}

function periodic<T>(period: number, value: T): Stream<T> {
  return new Stream<T>(new PeriodicSource(period, value));
}

export {periodic, PeriodicSource}
