import {Stream} from '../Stream';
import {Source} from './Source';
import {Sink} from '../sink/Sink';
import {Scheduler} from '../scheduler/Scheduler';
import * as dispose from '../disposable/dispose';
import {PropagateTask} from '../scheduler/PropagateTask';

function disposeEmpty(task) {
  return task.dispose();
}

class EmptySource implements Source<any> {
  constructor() {}

  run(sink: Sink<any>, scheduler: Scheduler) {
    const task = PropagateTask.end(void 0, sink);
    scheduler.asap(task);

    return dispose.create(disposeEmpty, task);
  }
}

export function empty() {
  return new Stream<any>(new EmptySource());
}

class NeverSource implements Source<any> {
  constructor() {}
  run() { return dispose.empty(); }
}

export function never() {
  return new Stream<any>(new NeverSource());
}
