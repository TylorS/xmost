import {Source} from './Source';
import {Sink} from '../sink/Sink';
import {Scheduler} from '../scheduler/Scheduler';
import {Task} from '../scheduler/Task';
import {Disposable} from '../disposable/Disposable';

import {Stream} from '../Stream';
import {PropagateTask} from '../scheduler/PropagateTask';

function produce(task, array, sink) {
  for (let i = 0, l = array.length; i < l && task.active; ++i) {
    sink.event(0, array[i]);
  }

  function end() {
    sink.end(0);
  }

  if (task.active) {
    end();
  }

}

function runProducer<T>(t: any, array: Array<any>, sink: Sink<T>) {
  produce(this, array, sink);
}

class FromProducer<T> implements Disposable<T> {
  private task: Task;
  constructor(private arr: Array<any>, private sink: Sink<T>,
              private scheduler: Scheduler) {
    this.task = new PropagateTask(runProducer, arr, sink);
    scheduler.asap(this.task);
  }

  dispose() {
    this.task.dispose();
  }
}

class FromSource<T> implements Source<T> {
  constructor(private arr: Array<any>) {
  }

  run(sink: Sink<T>, scheduler: Scheduler): Disposable<T> {
    const {arr} = this;
    return new FromProducer(arr, sink, scheduler);
  }
}

function from<T>(x: Array<T>) {
  return new Stream<T>(new FromSource<T>(x));
}

function of<T>(...x: Array<T>) {
  return from(x);
}

export {of, from, FromSource}
