import {Source} from '../Source';
import {Sink} from '../../sink/Sink';
import {Disposable} from '../../disposable/Disposable';
import {Scheduler} from '../../scheduler/Scheduler';

import MulticastDisposable from './MulticastDisposable';
import { append, remove, findIndex } from '../../util/array';
import { tryEvent, tryEnd } from '../../util/try';

function dispose<T>(disposable: Disposable<T>) {
  return disposable.dispose();
}

export default class MulticastSource<T> implements Source<T> {
  public sinks: Array<Sink<T>> = [];
  private _disposable: Disposable<T>;
  private stopID: any = null;
  constructor(public source: Source<T>) {
  }

  run(sink: Sink<T>, scheduler: Scheduler) {
    const n = this.add(sink);
    if (n === 1) {
      this._disposable = this.source.run(this, scheduler);
    }
    return new MulticastDisposable(this, sink);
  }

  _dispose() {
    const self = this;
    this.stopID = setTimeout(() => {
      const disposable = self._disposable;
      self._disposable = void 0;
      Promise.resolve(disposable).then(dispose);
    });
  }

  add(sink: Sink<T>) {
    this.sinks = append(sink, this.sinks);
    if (this.stopID !== null) {
      clearTimeout(this.stopID);
      this.stopID = null;
    }
    return this.sinks.length;
  }

  remove (sink) {
    this.sinks = remove(findIndex(sink, this.sinks), this.sinks);
    if (this.sinks.length === 0) {
      this.stopID = setTimeout(() => this._dispose(), 20);
    }
  }

  event (time, value) {
    const s = this.sinks;
    if (s.length === 1) {
      tryEvent(time, value, s[0]);
      return;
    }
    for (let i = 0; i < s.length; ++i) {
      tryEvent(time, value, s[i]);
    }
  }

  end(time, value) {
    debugger;
    const s = this.sinks;
    this.stopID = setTimeout(() => {
      if (s.length === 1) {
        tryEnd(time, value, s[0]);
        return;
      }
      for (let i = 0; i < s.length; ++i) {
        tryEnd(time, value, s[i]);
      }
    });
  }

  error(time, err) {
    const s = this.sinks;
    if (s.length === 1) {
      s[0].error(time, err);
      return;
    }
    for (let i = 0; i < s.length; ++i) {
      s[i].error(time, err);
    }
  }
}
