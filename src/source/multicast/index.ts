import {Stream} from '../../Stream';
import MulticastSource from './MulticastSource';

function multicast<T>(stream: Stream<T>) {
  const {source} = stream;
  return source instanceof MulticastSource
    ? stream
    : new Stream<T>(new MulticastSource(source));
}

export {multicast, MulticastSource}
