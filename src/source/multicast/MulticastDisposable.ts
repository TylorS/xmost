import {Sink} from '../../sink/Sink';

export default class MulticastDisposable<T> {
  constructor (public source: any, public sink: Sink<T>) {}

  dispose () {
    this.source.remove(this.sink);
  }
}
