import {Sink} from '../sink/Sink';
import {Scheduler} from '../scheduler/Scheduler';
import {Disposable} from '../disposable/Disposable';

export interface Source<T> {
  sinks?: Array<Sink<T>>;
  run(sink: Sink<T>, scheduler: Scheduler): Disposable<any>;
}
