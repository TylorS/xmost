// Type Signatures
import {Source} from './source/Source';
import {MulticastSource} from './source/multicast/index';

// Sources
import {of, from} from './source/FromSource';
import {periodic} from './source/PeriodicSource';
import {empty, never} from './source/EmptySource';

// Combinators
import {observe, drain} from './combinator/observe';
import {concat} from './combinator/concat';
import {continueWith} from './combinator/continueWith';
import {scan, reduce} from './combinator/fold';
import {slice, take, skip} from './combinator/slice';

export class Stream<T> {
  public source: Source<T>;
  constructor(source: Source<T>) {
    this.source = source instanceof MulticastSource ?
      source : new MulticastSource(source);
  }

  // sources
  static of = of;
  static from = from;
  static periodic = periodic;
  static empty = empty;
  static never = never;

  // combinators
  observe(f: (x: T) => any): Promise<T> {
    return observe(f, this);
  }

  drain(): Promise<T> {
    return drain(this);
  }

  concat(tail: Stream<T>) {
    return concat<T>(this, tail);
  }

  continueWith<R>(f: (x: T) => Stream<T>): Stream<T> {
    return continueWith<T>(f, this);
  }

  scan<T>(f: (acc: T, x: any) => T, initial: T): Stream<T> {
    return scan(f, initial, this);
  }

  reduce<R>(f: (acc: T, x: any) => R, initial): Promise<R> {
    return reduce(f, initial, this);
  }

  slice(start: number, end: number) {
    return slice(start, end, this);
  }

  take(n: number) {
    return take(n, this);
  }

  skip(n: number) {
    return skip(n, this);
  }
}
