import {Source} from '../source/Source';
import {Sink} from '../sink/Sink';
import {Scheduler} from '../scheduler/Scheduler';
import {Disposable} from '../disposable/Disposable';

import {Stream} from '../Stream';
import {once, promised, tryDispose} from '../disposable/dispose';
import {isPromise} from '../util/isPromise';

class ContinueWithSink<T> implements Sink<T> {
  private active: boolean = true;
  public disposable: Disposable<T>;
  constructor(private f: (x: any) => Stream<any>, private source: Source<T>,
              private sink: Sink<T>, private scheduler: Scheduler) {
      this.disposable = once(source.run(this, scheduler));
  }

  event(t: number, x: any) {
    if (!this.active) { return; }
    this.sink.event(t, x);
  }

  error(t: number, e: any) {
    this.sink.error(t, e);
  }

  end(t: number, x: any) {
    if (!this.active) { return; }

    const result = tryDispose(t, this.disposable, this.sink);
    this.disposable = isPromise(result) ?
      promised(this._thenContinue(result, x)) :
      this._continue(this.f, x);
  }

  private _thenContinue<T>(p: Promise<T>, x: any) {
    const self = this;
    return p.then(function thenContinue() {
      return self._continue(self.f, x);
    });
  }

  private _continue(f, x) {
    return f(x).source.run(this.sink, this.scheduler);
  }

  dispose() {
    this.active = false;
    return this.disposable.dispose();
  }
}

class ContinueWith<T> {
  constructor(private f: (x: T) => Stream<T>, private source: Source<any>) {
  }

  run(sink: Sink<T>, scheduler: Scheduler) {
    return new ContinueWithSink(this.f, this.source, sink, scheduler);
  }
}

function continueWith<T>(f: (x: T) => Stream<T>, stream: Stream<T>) {
  return new Stream<T>(new ContinueWith<T>(f, stream.source));
}

export {continueWith}
