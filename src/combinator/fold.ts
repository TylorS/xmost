import {Stream} from '../Stream';
import {Source} from '../source/Source';
import {Sink} from '../sink/Sink';
import {Pipe} from '../sink/Pipe';
import {Scheduler} from '../scheduler/Scheduler';

import {startWith} from './startWith';
import {withDefaultScheduler} from '../scheduler/runSource';

export declare type accumulateFn = (acc: any, x: any) => any;

class Accumulate<T> {
  constructor(private sinkType: any, private f: accumulateFn,
              private value: any, private source: Source<T>) {
  }

  run(sink: Sink<T>, scheduler: Scheduler) {
    const {sinkType, f, value} = this;
    return this.source.run(new sinkType(f, value, sink), scheduler);
  }
}

class ScanSink<T> extends Pipe<T> {
  constructor(protected f: accumulateFn,
              protected value: any,
              sink: Sink<T>) {
      super(sink);
  }

  event(t: number, x: any) {
    const {f} = this;
    this.value = f(this.value, x);
    this.sink.event(t, this.value);
  }
}

function scan<T>(f: accumulateFn, initial: T, stream: Stream<any>): Stream<T> {
  return startWith(
    initial,
    new Stream<T>(new Accumulate<T>(ScanSink, f, initial, stream.source))
  );
}

class AccumulateSink<T> extends ScanSink<T> {
  constructor(f: accumulateFn,
              value: any,
              sink: Sink<T>) {
    super(f, value, sink);
  }

  end(t) {
    this.sink.end(t, this.value);
  }
}

function reduce<T>(f: accumulateFn, initial: T, stream: Stream<any>): Promise<T> {
  return withDefaultScheduler(
    () => {}, new Accumulate(AccumulateSink, f, initial, stream.source));
}

export {scan, reduce}
