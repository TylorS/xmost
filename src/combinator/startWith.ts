import {Stream} from '../Stream';
import {of} from '../source/FromSource';
import {concat} from './concat';

export function startWith<T> (x: T, stream: Stream<T>): Stream<T> {
  return concat(of(x), stream);
}
