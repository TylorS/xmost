import {Stream} from '../Stream';
import {Source} from '../source/Source';
import {Sink} from '../sink/Sink';
import {Scheduler} from '../scheduler/Scheduler';
import {Disposable} from '../disposable/Disposable';
import {once} from '../disposable/dispose';
import {empty} from '../source/EmptySource';

class SliceSink<T> implements Sink<T> {
  public disposable: Disposable<T>;
  constructor(private skip: number, private take: number,
              private source: Source<T>, private sink: Sink<T>,
              scheduler: Scheduler) {
    this.disposable = once(source.run(this, scheduler));
  }

  event(t: number, x: any) {
    if (this.skip > 0) {
      this.skip -= 1;
      return;
    }

    if (this.take === 0) { return; }

    this.take -= 1;
    if (this.take === 0 && this.source.sinks.length <= 0) {
      this.sink.end(t, x);
    }
    this.sink.event(t, x);
  }

  error(t, e) {
    this.sink.error(t, e);
  }

  end(t, x) {
    this.sink.end(t, x);
  }

  dispose() {
    this.disposable.dispose();
  }
}

class Slice<T> implements Source<T> {
  private take: number;
  constructor(private skip: number, max: number, private source: Source<T>) {
    this.take = max - skip;
  }

  run(sink: Sink<T>, scheduler: Scheduler) {
    return new SliceSink(this.skip, this.take, this.source, sink, scheduler);
  }
}

export function slice<T>(start: number, end: number, stream: Stream<T>) {
  return end <= start ?
    empty() :
    new Stream<T>(new Slice<T>(start, end, stream.source));
}

export function take<T>(n: number, stream: Stream<T>) {
  return slice<T>(0, n, stream);
}

export function skip<T>(n: number, stream: Stream<T>) {
  return slice<T>(n, Infinity, stream);
}
