import {Stream} from '../Stream';
import {continueWith} from './continueWith';

export function concat<T>(left: Stream<T>, right: Stream<T>) {
  return continueWith<T>(() => right, left);
}
