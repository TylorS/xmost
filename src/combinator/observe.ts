import {Stream} from '../Stream';
import {withDefaultScheduler} from '../scheduler/runSource';

function observe<T>(f: (x?: T) => any, stream: Stream<T>): Promise<T> {
  return withDefaultScheduler(f, stream.source);
}

function drain<T>(stream: Stream<T>): Promise<T> {
  return observe(() => {}, stream);
}

export {observe, drain}
