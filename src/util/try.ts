import {Sink} from '../sink/Sink';

function tryEvent<T>(t: number, x: any, sink: Sink<T>) {
  try {
    sink.event(t, x);
  } catch (e) {
    sink.error(t, e);
  }
}

function tryEnd<T>(t: number, x: any, sink: Sink<T>) {
  try {
    sink.end(t, x);
  } catch (e) {
    sink.error(t, e);
  }
}

export {tryEvent, tryEnd}
