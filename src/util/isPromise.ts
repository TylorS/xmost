export function isPromise<T>(p: Promise<T>): boolean {
  return p !== null &&
    typeof p === 'object' &&
    typeof p.then === 'function';
}
