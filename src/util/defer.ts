import {Task} from '../scheduler/Task';

function runTask(task: Task) {
  try {
    return task.run();
  } catch (e) {
    return task.error(e);
  }
}

function defer(task: Task) {
  return Promise.resolve(task).then(runTask);
}

export {defer}
