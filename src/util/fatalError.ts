export function fatalError(e) {
  setTimeout(function() {
    throw e;
  }, 0);
}
