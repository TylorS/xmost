// id :: a -> a
export function id(x: any): any { return x; }

// compose :: (b -> c) -> (a -> b) -> (a -> c)
export function compose<T>(f: (a: T) => T, g: (b: T) => T) {
  return function composedFunction(x: T): T {
    return f(g(x));
  };
}

// apply :: (a -> b) -> a -> b
export function apply<T>(f: (x: T) => any, x: T) {
  return f(x);
}

// curry2 :: ((a, b) -> c) -> (a -> b -> c)
export function curry2 (f: (a: any, b: any) => any): any {
  function curried (a: any, b: any) {
    switch (arguments.length) {
      case 0: return curried;
      case 1: return b => f(a, b);
      default: return f(a, b);
    }
  }
  return curried;
}

// curry3 :: ((a, b, c) -> d) -> (a -> b -> c -> d)
export function curry3 (f: (a: any, b: any, c: any) => any) {
  function curried (a, b, c) { // eslint-disable-line complexity
    switch (arguments.length) {
      case 0: return curried;
      case 1: return curry2((b, c) => f(a, b, c));
      case 2: return c => f(a, b, c);
      default: return f(a, b, c);
    }
  }
  return curried;
}
