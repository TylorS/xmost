import {Disposable} from './Disposable';

export class SettableDisposable<T> implements Disposable<T> {
  private disposable: Disposable<T>;
  private disposed: boolean = false;
  private resolve: (x: any) => any;
  private result: T | Promise<T> | void;
  constructor() {
    const self = this;
    this.result = new Promise<T>(resolve => {
      self.resolve = resolve;
    });
  }

  setDisposable(disposable: Disposable<T>) {
    this.disposable = disposable;
    if (this.disposed) {
      this.resolve(disposable.dispose());
    }
  }

  dispose() {
    if (this.disposed) {
      return this.result;
    }

    this.disposed = true;

    if (this.disposable) {
      this.result = this.disposable.dispose();
    }

    return this.result;
  }
}
