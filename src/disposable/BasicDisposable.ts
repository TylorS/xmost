import {Disposable} from './Disposable';

export class BasicDisposable<T> implements Disposable<T> {
  constructor(private disposeFn: (x?: any) => any, private data: any) {}

  dispose() {
    this.disposeFn(this.data);
  }
}
