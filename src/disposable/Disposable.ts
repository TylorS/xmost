export interface Disposable<T> {
  dispose(x?: any): T | Promise<T> | void ;
}
