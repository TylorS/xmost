// Type Signatures
import {Sink} from '../sink/Sink';
import {Disposable} from './Disposable';

import {BasicDisposable} from './BasicDisposable';
import {SettableDisposable} from './SettableDisposable';

import {isPromise} from '../util/isPromise';
import {map} from '../util/array';
import {id} from '../util/function';

function disposeSafely<T>(disposable: Disposable<T>): any {
  try {
    return disposable.dispose();
  } catch (e) {
    return Promise.reject(e);
  }
}

function tryDispose<T>(t: number, disposable: Disposable<T>, sink: Sink<T>) {
  const result = disposeSafely(disposable);
  return isPromise(result) ?
    result.catch(e => { sink.error(t, e); }) :
    result;
}

function memoized<T>(disposable: Disposable<T>): any {
  return { disposed: false, disposable: disposable, value: void 0 };
}

function disposeMemoized(memoized: any): any {
  if (!memoized.disposed) {
    memoized.disposed = true;
    memoized.value = disposeSafely(memoized.disposable);
    memoized.disposable = void 0;
  }

  return memoized.value;
}

function once<T>(disposable: Disposable<T>): any {
  return new BasicDisposable(disposeMemoized, memoized(disposable));
}

function create<T>(dispose: (x?: any) => any, data: any): Disposable<T> {
  return once(new BasicDisposable(dispose, data));
}

function empty<T>(): Disposable<T> {
  return new BasicDisposable(id, void 0);
}

function disposeAll<T>(disposables: Disposable<T>[]): Promise<any> {
  return Promise.all(map(disposeSafely, disposables));
}

function all<T>(disposables: Disposable<T>[]): any {
  return create(disposeAll, disposables);
}

function disposeOne(disposable): any {
  return disposable.dispose();
}

function disposePromise(disposablePromise): any {
  return disposablePromise.then(disposeOne);
}

function promised(disposablePromise): any {
  return create(disposePromise, disposablePromise);
}

function settable<T>(): SettableDisposable<T> {
  return new SettableDisposable<T>();
}

export {
  tryDispose,
  create,
  once,
  empty,
  all,
  settable,
  promised
}
