import {Timer} from './Timer';
import {defer} from '../util/defer';
import {BasicTask} from './Task';

function isTask(t: any) {
  return typeof t.run === 'function' &&
    typeof t.error === 'function' &&
    typeof t.cancel === 'function';
}

function runAsTask(f) {
  const task = new BasicTask(f);
  defer(task);
  return task;
}

const nodeTimer: Timer = {
  now: Date.now,
  setTimer(f, dt) {
    return dt < 0 ? runAsTask(f) : setTimeout(f, dt);
  },
  clearTimer(t) {
    return isTask(t) ? t.cancel() : clearTimeout(t);
  }
};

export {nodeTimer}
