import {Task} from './Task';
import {fatalError} from '../util/fatalError';

function error(t, e, sink) {
  sink.error(t, e);
}

function emit(t, x, sink) {
  sink.event(t, x);
}

function end(t, x, sink) {
  sink.end(t, x);
}

export class PropagateTask implements Task {
  public active: boolean = true;
  constructor(public _run, public value, public sink) {
  }

  static event(value, sink) {
    return new PropagateTask(emit, value, sink);
  }

  static end(value, sink) {
    return new PropagateTask(end, value, sink);
  }

  static error(value, sink) {
    return new PropagateTask(error, value, sink);
  }

  dispose() {
    this.active = false;
  }

  run(t) {
    if (!this.active) { return; }
    this._run(t, this.value, this.sink);
  }

  error(e) {
    if (!this.active) {
      return fatalError(e);
    }
    this.sink(Date.now(), e);
  }

  cancel() {
    this.active = false;
  }
}
