import {Observer} from '../sink/Observer';
import {settable} from '../disposable/dispose';
import {defaultScheduler} from './defaultScheduler';

export function runSource(f, source, scheduler, resolve, reject) {
  const disposable = settable();
  const observer = new Observer(f, resolve, reject, disposable);
  disposable.setDisposable(source.run(observer, scheduler));
}

export function withScheduler<T>(f, source, scheduler) {
  return new Promise<T>(function (resolve, reject) {
    runSource(f, source, scheduler, resolve, reject);
  });
}

export function withDefaultScheduler(f, source) {
  return withScheduler(f, source, defaultScheduler);
}
