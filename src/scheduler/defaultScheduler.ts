import {BasicScheduler} from './Scheduler';
import {timeoutTimer} from './timeoutTimer';
import {nodeTimer} from './nodeTimer';

const isNode = typeof process === 'object' &&
  typeof process.nextTick === 'function';

const defaultScheduler = new BasicScheduler(isNode ? nodeTimer : timeoutTimer);

export {defaultScheduler}
