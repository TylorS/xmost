export interface Task {
  active?: boolean;
  run(t?: any): any;
  error(e: any): any;
  cancel(): void;
  dispose(): void;
}

export class BasicTask implements Task {
  active: boolean = false;
  constructor(private f: Function) {}

  run(t: any) {
    if (!this.active) { return void 0; }
    const {f} = this;
    return f();
  }

  error(e: Error) {
    throw e;
  }

  cancel() {
    this.active = false;
  }

  dispose() {}
}
