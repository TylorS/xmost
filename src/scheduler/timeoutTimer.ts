import {Timer} from './Timer';

const timeoutTimer: Timer = {
  now: Date.now,
  setTimer: function(f, dt) {
    return setTimeout(f, dt);
  },
  clearTimer: function(t) {
    return clearTimeout(t);
  }
};

export {timeoutTimer}
