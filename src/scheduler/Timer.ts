export declare type Timer = {
  now(): number;
  setTimer(f: Function, delayTime: number): any;
  clearTimer(id: any): any;
}
