import {Task} from './Task';
import {Scheduler} from './Scheduler';

export class ScheduledTask implements Task {
  public active: boolean = true;
  constructor(public time: number, public period: number,
              private task: any, private scheduler: Scheduler) {
  }

  run() {
    this.active = true;
    this.task.run(this.time);
  }

  error(e: any) {
    return this.task.error(this.time, e);
  }

  cancel() {
    this.scheduler.cancel(this);
    return this.task.dispose();
  }

  dispose() {
    this.active = false;
  }
}
