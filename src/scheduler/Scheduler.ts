import {Timer} from './Timer';
import {Task} from './Task';
import {ScheduledTask} from './ScheduledTask';

export interface Scheduler {
  timer: Timer;
  now(): number;
  asap(task: Task): ScheduledTask;
  delay(delay: number, task: Task): ScheduledTask;
  periodic(period: number, task: Task): ScheduledTask;
  schedule(delay: number, period: number, task: Task): ScheduledTask;
  cancel(task: Task): void;
  cancelAll(f: Function): void;
}

import {findIndex, removeAll} from '../util/array';

function removeAllFrom(f, timeslot) {
  timeslot.events = removeAll(f, timeslot.events);
}

function newTimeslot(t, events) {
  return { time: t, events: events };
}

function runTask(task) {
  try {
    return task.run();
  } catch (e) {
    return task.error(e);
  }
}

function runTasks({events}, tasks) {
  for (let i = 0; i < events.length; ++i) {
    const task = events[i];

    if (task.active) {
      runTask(task);

      // Reschedule periodic repeating tasks
      // Check active again, since a task may have canceled itself
      if (task.period >= 0) {
        task.time = task.time + task.period;
        insertByTime(task, tasks);
      }
    }
  }

  return tasks;
}

function insertByTime(task, timeslots) {
  const l = timeslots.length;

  if (l === 0) {
    timeslots.push(newTimeslot(task.time, [task]));
    return;
  }

  const i = binarySearch(task.time, timeslots);

  if (i >= l) {
    timeslots.push(newTimeslot(task.time, [task]));
  } else if (task.time === timeslots[i].time) {
    timeslots[i].events.push(task);
  } else {
    timeslots.splice(i, 0, newTimeslot(task.time, [task]));
  }
}

function binarySearch(t: any, sortedArray: Array<any>): any {
  let lo = 0;
  let hi = sortedArray.length;
  let mid, y;

  while (lo < hi) {
    mid = Math.floor((lo + hi) / 2);
    y = sortedArray[mid];

    if (t === y.time) {
      return mid;
    } else if (t < y.time) {
      hi = mid;
    } else {
      lo = mid + 1;
    }
  }
  return hi;
}

export class BasicScheduler implements Scheduler {
  private _timer: any = null;
  private _nextArrival: number = 0;
  private _tasks: Array<any> = [];
  private _runReadyTasksBound: Function;
  constructor(public timer: Timer) {
    const self = this;
    this._runReadyTasksBound = function runReadyTasksBound() {
      self._runReadyTasks(self.now());
    };
  }

  now() {
    return this.timer.now();
  }

  asap(task: Task) {
    return this.schedule(0, -1, task);
  }

  delay(delay: number, task: Task) {
    return this.schedule(delay, -1, task);
  }

  periodic(period: number, task: Task) {
    return this.schedule(0, period, task);
  }

  schedule(delay: number, period: number, task: Task) {
    const now = this.now();
    const st = new ScheduledTask(+(now) + Math.max(0, delay), period, task, this);
    insertByTime(st, this._tasks);
    this._scheduleNextRun(+(now));
    return st;
  }

  cancel(task: ScheduledTask) {
    task.active = false;
    const i = binarySearch(task.time, this._tasks);

    if (i >= 0 && i < this._tasks.length) {
      const at = findIndex(task, this._tasks[i].events);
      if (at >= 0) {
        this._tasks[i].events.splice(at, 1);
        this._reschedule();
      }
    }
  }

  cancelAll(f: Function) {
    for (let i = 0; i < this._tasks.length; ++i) {
      removeAllFrom(f, this._tasks[i]);
    }
    this._reschedule();
  }

  _reschedule() {
    if (this._tasks.length === 0) {
      this._unschedule();
    } else {
      this._scheduleNextRun(this.now());
    }
  }

  _unschedule() {
    this.timer.clearTimer(this._timer);
    this._timer = null;
  }

  _scheduleNextRun(now: number) {
    if (this._tasks.length === 0) {
      return;
    }

    const nextArrival = this._tasks[0].time;

    if (this._timer === null) {
      this._scheduleNextArrival(nextArrival, now);
    } else if (nextArrival < this._nextArrival) {
      this._unschedule();
      this._scheduleNextArrival(nextArrival, now);
    }
  }

  _scheduleNextArrival(nextArrival: any, now: any) {
    this._nextArrival = nextArrival;
    const delay = Math.max(0, nextArrival - now);
    this._timer = this.timer.setTimer(this._runReadyTasksBound, delay);
  }

  _runReadyTasks(now: number) {
    this._timer = null;

    this._tasks = this._findAndRunTasks(now);

    this._scheduleNextRun(this.now());
  }

  _findAndRunTasks(now: number) {
    const tasks = this._tasks;
    const l = tasks.length;
    let i = 0;

    while (i < l && tasks[i].time <= now) {
      ++i;
    }

    this._tasks = tasks.slice(i);

    // Run all ready tasks
    for (let j = 0; j < i; ++j) {
      this._tasks = runTasks(tasks[j], this._tasks);
    }
    return this._tasks;
  }
}
