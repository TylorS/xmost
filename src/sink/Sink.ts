import {Disposable} from '../disposable/Disposable';
export interface Sink<T> {
  event(t: number, x: any): void;
  end(t: number, x: any): void;
  error(t: number, err: any): void;
  disposable?: Disposable<T>;
  dispose?: () => any;
}
