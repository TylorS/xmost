import {Sink} from './Sink';

export class Pipe<T> implements Sink<T> {
  public stopID: any = null;
  constructor(protected sink: Sink<T>) {}

  event(t, x) {
    if (this.stopID !== null) {
      clearTimeout(this.stopID);
      this.stopID = null;
    }
    this.sink.event(t, x);
  }

  end(t, x) {
    const self = this;
    this.stopID = setTimeout(() => {
       self.sink.end(t, x);
       self.dispose();
    });
  }

  error(t, e) {
    this.sink.error(t, e);
  }

  dispose() {
    if (typeof this.sink.dispose === 'function') {
      this.sink.dispose();
    }
  }
}
