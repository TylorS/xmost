import {Sink} from './Sink';

function disposeThen(end, error, disposable, x) {
  Promise.resolve(disposable.dispose()).then(() => { end(x); }, error);
}

export class Observer<T> implements Sink<T> {
  public active: boolean = true;
  constructor(private _event, private _end,
              private _error, public disposable) {
  }

  event(t: number, x: any) {
    if (!this.active) { return; }
    this._event(x);
  }

  end(t: number, x: any) {
    if (!this.active) { return; }
    disposeThen(this._end, this._error, this.disposable, x);
    this.active = false;
  }

  error(t: number, x: any) {
    if (!this.active) { return; }
    this.active = false;
    disposeThen(this._error, this._error, this.disposable, x);
  }

  dispose() {}
}
