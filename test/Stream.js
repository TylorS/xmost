import assert from 'assert'
import {Stream} from '../lib/index';

describe('Stream', () => {
  it('should have from', done => {
    const arr = [1, 2, 3];
    const stream = Stream.from([1,2,3])
    stream.observe(x => {
      assert.strictEqual(x, arr.shift());
      if (arr.length === 0) { done() }
    })
  })
  
  it('should only take x', () => {
    const stream = Stream.from([1, 2, 3, 4]);
    
    const arr = [];
    return stream.take(2)
      .observe(x => arr.push(x))
      .then(() => {
        assert.strictEqual(arr.length, 2);
      })
  })
  
  it('should use continue when unlistened and relistened synchronously', function(done) {
    const add = (x, y) => x + y;
    const stream = Stream.periodic(100, 1).scan(add, 0);

    const expected = [0, 1, 2];

    function next(x) {
      console.log(x);
      assert.strictEqual(x, expected.shift());
      if (expected.length === 0) { done(); }
    }
    function error(err) {
      done(err);
    }
    function complete() {
      done(new Error('this should never be called'));
    }
    stream.take(2).observe(next).then(complete).catch(error);

    setTimeout(() => {
      stream.observe(next);
    }, 100)
  })
})
