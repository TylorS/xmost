(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

var index_1 = require('./source/multicast/index');
// Sources
var FromSource_1 = require('./source/FromSource');
var PeriodicSource_1 = require('./source/PeriodicSource');
var EmptySource_1 = require('./source/EmptySource');
// Combinators
var observe_1 = require('./combinator/observe');
var concat_1 = require('./combinator/concat');
var continueWith_1 = require('./combinator/continueWith');
var fold_1 = require('./combinator/fold');
var slice_1 = require('./combinator/slice');
var Stream = function () {
    function Stream(source) {
        this.source = source instanceof index_1.MulticastSource ? source : new index_1.MulticastSource(source);
    }
    // combinators
    Stream.prototype.observe = function (f) {
        return observe_1.observe(f, this);
    };
    Stream.prototype.drain = function () {
        return observe_1.drain(this);
    };
    Stream.prototype.concat = function (tail) {
        return concat_1.concat(this, tail);
    };
    Stream.prototype.continueWith = function (f) {
        return continueWith_1.continueWith(f, this);
    };
    Stream.prototype.scan = function (f, initial) {
        return fold_1.scan(f, initial, this);
    };
    Stream.prototype.reduce = function (f, initial) {
        return fold_1.reduce(f, initial, this);
    };
    Stream.prototype.slice = function (start, end) {
        return slice_1.slice(start, end, this);
    };
    Stream.prototype.take = function (n) {
        return slice_1.take(n, this);
    };
    Stream.prototype.skip = function (n) {
        return slice_1.skip(n, this);
    };
    // sources
    Stream.of = FromSource_1.of;
    Stream.from = FromSource_1.from;
    Stream.periodic = PeriodicSource_1.periodic;
    Stream.empty = EmptySource_1.empty;
    Stream.never = EmptySource_1.never;
    return Stream;
}();
exports.Stream = Stream;


},{"./combinator/concat":2,"./combinator/continueWith":3,"./combinator/fold":4,"./combinator/observe":5,"./combinator/slice":6,"./source/EmptySource":22,"./source/FromSource":23,"./source/PeriodicSource":24,"./source/multicast/index":27}],2:[function(require,module,exports){
"use strict";

var continueWith_1 = require('./continueWith');
function concat(left, right) {
    return continueWith_1.continueWith(function () {
        return right;
    }, left);
}
exports.concat = concat;


},{"./continueWith":3}],3:[function(require,module,exports){
"use strict";

var Stream_1 = require('../Stream');
var dispose_1 = require('../disposable/dispose');
var isPromise_1 = require('../util/isPromise');
var ContinueWithSink = function () {
    function ContinueWithSink(f, source, sink, scheduler) {
        this.f = f;
        this.source = source;
        this.sink = sink;
        this.scheduler = scheduler;
        this.active = true;
        this.disposable = dispose_1.once(source.run(this, scheduler));
    }
    ContinueWithSink.prototype.event = function (t, x) {
        if (!this.active) {
            return;
        }
        this.sink.event(t, x);
    };
    ContinueWithSink.prototype.error = function (t, e) {
        this.sink.error(t, e);
    };
    ContinueWithSink.prototype.end = function (t, x) {
        if (!this.active) {
            return;
        }
        var result = dispose_1.tryDispose(t, this.disposable, this.sink);
        this.disposable = isPromise_1.isPromise(result) ? dispose_1.promised(this._thenContinue(result, x)) : this._continue(this.f, x);
    };
    ContinueWithSink.prototype._thenContinue = function (p, x) {
        var self = this;
        return p.then(function thenContinue() {
            return self._continue(self.f, x);
        });
    };
    ContinueWithSink.prototype._continue = function (f, x) {
        return f(x).source.run(this.sink, this.scheduler);
    };
    ContinueWithSink.prototype.dispose = function () {
        this.active = false;
        return this.disposable.dispose();
    };
    return ContinueWithSink;
}();
var ContinueWith = function () {
    function ContinueWith(f, source) {
        this.f = f;
        this.source = source;
    }
    ContinueWith.prototype.run = function (sink, scheduler) {
        return new ContinueWithSink(this.f, this.source, sink, scheduler);
    };
    return ContinueWith;
}();
function continueWith(f, stream) {
    return new Stream_1.Stream(new ContinueWith(f, stream.source));
}
exports.continueWith = continueWith;


},{"../Stream":1,"../disposable/dispose":10,"../util/isPromise":32}],4:[function(require,module,exports){
"use strict";

var __extends = undefined && undefined.__extends || function (d, b) {
    for (var p in b) {
        if (b.hasOwnProperty(p)) d[p] = b[p];
    }function __() {
        this.constructor = d;
    }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Stream_1 = require('../Stream');
var Pipe_1 = require('../sink/Pipe');
var startWith_1 = require('./startWith');
var runSource_1 = require('../scheduler/runSource');
var Accumulate = function () {
    function Accumulate(sinkType, f, value, source) {
        this.sinkType = sinkType;
        this.f = f;
        this.value = value;
        this.source = source;
    }
    Accumulate.prototype.run = function (sink, scheduler) {
        var _a = this,
            sinkType = _a.sinkType,
            f = _a.f,
            value = _a.value;
        return this.source.run(new sinkType(f, value, sink), scheduler);
    };
    return Accumulate;
}();
var ScanSink = function (_super) {
    __extends(ScanSink, _super);
    function ScanSink(f, value, sink) {
        _super.call(this, sink);
        this.f = f;
        this.value = value;
    }
    ScanSink.prototype.event = function (t, x) {
        var f = this.f;
        this.value = f(this.value, x);
        this.sink.event(t, this.value);
    };
    return ScanSink;
}(Pipe_1.Pipe);
function scan(f, initial, stream) {
    return startWith_1.startWith(initial, new Stream_1.Stream(new Accumulate(ScanSink, f, initial, stream.source)));
}
exports.scan = scan;
var AccumulateSink = function (_super) {
    __extends(AccumulateSink, _super);
    function AccumulateSink(f, value, sink) {
        _super.call(this, f, value, sink);
    }
    AccumulateSink.prototype.end = function (t) {
        this.sink.end(t, this.value);
    };
    return AccumulateSink;
}(ScanSink);
function reduce(f, initial, stream) {
    return runSource_1.withDefaultScheduler(function () {}, new Accumulate(AccumulateSink, f, initial, stream.source));
}
exports.reduce = reduce;


},{"../Stream":1,"../scheduler/runSource":18,"../sink/Pipe":21,"./startWith":7}],5:[function(require,module,exports){
"use strict";

var runSource_1 = require('../scheduler/runSource');
function observe(f, stream) {
    return runSource_1.withDefaultScheduler(f, stream.source);
}
exports.observe = observe;
function drain(stream) {
    return observe(function () {}, stream);
}
exports.drain = drain;


},{"../scheduler/runSource":18}],6:[function(require,module,exports){
"use strict";

var Stream_1 = require('../Stream');
var dispose_1 = require('../disposable/dispose');
var EmptySource_1 = require('../source/EmptySource');
var SliceSink = function () {
    function SliceSink(skip, take, source, sink, scheduler) {
        this.skip = skip;
        this.take = take;
        this.source = source;
        this.sink = sink;
        this.disposable = dispose_1.once(source.run(this, scheduler));
    }
    SliceSink.prototype.event = function (t, x) {
        if (this.skip > 0) {
            this.skip -= 1;
            return;
        }
        if (this.take === 0) {
            return;
        }
        this.take -= 1;
        if (this.take === 0 && this.source.sinks.length <= 0) {
            this.sink.end(t, x);
        }
        this.sink.event(t, x);
    };
    SliceSink.prototype.error = function (t, e) {
        this.sink.error(t, e);
    };
    SliceSink.prototype.end = function (t, x) {
        this.sink.end(t, x);
    };
    SliceSink.prototype.dispose = function () {
        this.disposable.dispose();
    };
    return SliceSink;
}();
var Slice = function () {
    function Slice(skip, max, source) {
        this.skip = skip;
        this.source = source;
        this.take = max - skip;
    }
    Slice.prototype.run = function (sink, scheduler) {
        return new SliceSink(this.skip, this.take, this.source, sink, scheduler);
    };
    return Slice;
}();
function slice(start, end, stream) {
    return end <= start ? EmptySource_1.empty() : new Stream_1.Stream(new Slice(start, end, stream.source));
}
exports.slice = slice;
function take(n, stream) {
    return slice(0, n, stream);
}
exports.take = take;
function skip(n, stream) {
    return slice(n, Infinity, stream);
}
exports.skip = skip;


},{"../Stream":1,"../disposable/dispose":10,"../source/EmptySource":22}],7:[function(require,module,exports){
"use strict";

var FromSource_1 = require('../source/FromSource');
var concat_1 = require('./concat');
function startWith(x, stream) {
    return concat_1.concat(FromSource_1.of(x), stream);
}
exports.startWith = startWith;


},{"../source/FromSource":23,"./concat":2}],8:[function(require,module,exports){
"use strict";

var BasicDisposable = function () {
    function BasicDisposable(disposeFn, data) {
        this.disposeFn = disposeFn;
        this.data = data;
    }
    BasicDisposable.prototype.dispose = function () {
        this.disposeFn(this.data);
    };
    return BasicDisposable;
}();
exports.BasicDisposable = BasicDisposable;


},{}],9:[function(require,module,exports){
"use strict";

var SettableDisposable = function () {
    function SettableDisposable() {
        this.disposed = false;
        var self = this;
        this.result = new Promise(function (resolve) {
            self.resolve = resolve;
        });
    }
    SettableDisposable.prototype.setDisposable = function (disposable) {
        this.disposable = disposable;
        if (this.disposed) {
            this.resolve(disposable.dispose());
        }
    };
    SettableDisposable.prototype.dispose = function () {
        if (this.disposed) {
            return this.result;
        }
        this.disposed = true;
        if (this.disposable) {
            this.result = this.disposable.dispose();
        }
        return this.result;
    };
    return SettableDisposable;
}();
exports.SettableDisposable = SettableDisposable;


},{}],10:[function(require,module,exports){
"use strict";

var BasicDisposable_1 = require('./BasicDisposable');
var SettableDisposable_1 = require('./SettableDisposable');
var isPromise_1 = require('../util/isPromise');
var array_1 = require('../util/array');
var function_1 = require('../util/function');
function disposeSafely(disposable) {
    try {
        return disposable.dispose();
    } catch (e) {
        return Promise.reject(e);
    }
}
function tryDispose(t, disposable, sink) {
    var result = disposeSafely(disposable);
    return isPromise_1.isPromise(result) ? result.catch(function (e) {
        sink.error(t, e);
    }) : result;
}
exports.tryDispose = tryDispose;
function memoized(disposable) {
    return { disposed: false, disposable: disposable, value: void 0 };
}
function disposeMemoized(memoized) {
    if (!memoized.disposed) {
        memoized.disposed = true;
        memoized.value = disposeSafely(memoized.disposable);
        memoized.disposable = void 0;
    }
    return memoized.value;
}
function once(disposable) {
    return new BasicDisposable_1.BasicDisposable(disposeMemoized, memoized(disposable));
}
exports.once = once;
function create(dispose, data) {
    return once(new BasicDisposable_1.BasicDisposable(dispose, data));
}
exports.create = create;
function empty() {
    return new BasicDisposable_1.BasicDisposable(function_1.id, void 0);
}
exports.empty = empty;
function disposeAll(disposables) {
    return Promise.all(array_1.map(disposeSafely, disposables));
}
function all(disposables) {
    return create(disposeAll, disposables);
}
exports.all = all;
function disposeOne(disposable) {
    return disposable.dispose();
}
function disposePromise(disposablePromise) {
    return disposablePromise.then(disposeOne);
}
function promised(disposablePromise) {
    return create(disposePromise, disposablePromise);
}
exports.promised = promised;
function settable() {
    return new SettableDisposable_1.SettableDisposable();
}
exports.settable = settable;


},{"../util/array":28,"../util/function":31,"../util/isPromise":32,"./BasicDisposable":8,"./SettableDisposable":9}],11:[function(require,module,exports){
"use strict";
// Type Signatures
// Stream

var Stream_1 = require('./Stream');
exports.Stream = Stream_1.Stream;
// sources
var FromSource_1 = require('./source/FromSource');
exports.of = FromSource_1.of;
exports.from = FromSource_1.from;
var PeriodicSource_1 = require('./source/PeriodicSource');
exports.periodic = PeriodicSource_1.periodic;
var EmptySource_1 = require('./source/EmptySource');
exports.empty = EmptySource_1.empty;
exports.never = EmptySource_1.never;
// combinators
var observe_1 = require('./combinator/observe');
exports.observe = observe_1.observe;
exports.drain = observe_1.drain;
var concat_1 = require('./combinator/concat');
exports.concat = concat_1.concat;
var continueWith_1 = require('./combinator/continueWith');
exports.continueWith = continueWith_1.continueWith;
var fold_1 = require('./combinator/fold');
exports.scan = fold_1.scan;
exports.reduce = fold_1.reduce;
var slice_1 = require('./combinator/slice');
exports.slice = slice_1.slice;
exports.take = slice_1.take;
exports.skip = slice_1.skip;


},{"./Stream":1,"./combinator/concat":2,"./combinator/continueWith":3,"./combinator/fold":4,"./combinator/observe":5,"./combinator/slice":6,"./source/EmptySource":22,"./source/FromSource":23,"./source/PeriodicSource":24}],12:[function(require,module,exports){
"use strict";

var fatalError_1 = require('../util/fatalError');
function error(t, e, sink) {
    sink.error(t, e);
}
function emit(t, x, sink) {
    sink.event(t, x);
}
function end(t, x, sink) {
    sink.end(t, x);
}
var PropagateTask = function () {
    function PropagateTask(_run, value, sink) {
        this._run = _run;
        this.value = value;
        this.sink = sink;
        this.active = true;
    }
    PropagateTask.event = function (value, sink) {
        return new PropagateTask(emit, value, sink);
    };
    PropagateTask.end = function (value, sink) {
        return new PropagateTask(end, value, sink);
    };
    PropagateTask.error = function (value, sink) {
        return new PropagateTask(error, value, sink);
    };
    PropagateTask.prototype.dispose = function () {
        this.active = false;
    };
    PropagateTask.prototype.run = function (t) {
        if (!this.active) {
            return;
        }
        this._run(t, this.value, this.sink);
    };
    PropagateTask.prototype.error = function (e) {
        if (!this.active) {
            return fatalError_1.fatalError(e);
        }
        this.sink(Date.now(), e);
    };
    PropagateTask.prototype.cancel = function () {
        this.active = false;
    };
    return PropagateTask;
}();
exports.PropagateTask = PropagateTask;


},{"../util/fatalError":30}],13:[function(require,module,exports){
"use strict";

var ScheduledTask = function () {
    function ScheduledTask(time, period, task, scheduler) {
        this.time = time;
        this.period = period;
        this.task = task;
        this.scheduler = scheduler;
        this.active = true;
    }
    ScheduledTask.prototype.run = function () {
        this.active = true;
        this.task.run(this.time);
    };
    ScheduledTask.prototype.error = function (e) {
        return this.task.error(this.time, e);
    };
    ScheduledTask.prototype.cancel = function () {
        this.scheduler.cancel(this);
        return this.task.dispose();
    };
    ScheduledTask.prototype.dispose = function () {
        this.active = false;
    };
    return ScheduledTask;
}();
exports.ScheduledTask = ScheduledTask;


},{}],14:[function(require,module,exports){
"use strict";

var ScheduledTask_1 = require('./ScheduledTask');
var array_1 = require('../util/array');
function removeAllFrom(f, timeslot) {
    timeslot.events = array_1.removeAll(f, timeslot.events);
}
function newTimeslot(t, events) {
    return { time: t, events: events };
}
function runTask(task) {
    try {
        return task.run();
    } catch (e) {
        return task.error(e);
    }
}
function runTasks(_a, tasks) {
    var events = _a.events;
    for (var i = 0; i < events.length; ++i) {
        var task = events[i];
        if (task.active) {
            runTask(task);
            // Reschedule periodic repeating tasks
            // Check active again, since a task may have canceled itself
            if (task.period >= 0) {
                task.time = task.time + task.period;
                insertByTime(task, tasks);
            }
        }
    }
    return tasks;
}
function insertByTime(task, timeslots) {
    var l = timeslots.length;
    if (l === 0) {
        timeslots.push(newTimeslot(task.time, [task]));
        return;
    }
    var i = binarySearch(task.time, timeslots);
    if (i >= l) {
        timeslots.push(newTimeslot(task.time, [task]));
    } else if (task.time === timeslots[i].time) {
        timeslots[i].events.push(task);
    } else {
        timeslots.splice(i, 0, newTimeslot(task.time, [task]));
    }
}
function binarySearch(t, sortedArray) {
    var lo = 0;
    var hi = sortedArray.length;
    var mid, y;
    while (lo < hi) {
        mid = Math.floor((lo + hi) / 2);
        y = sortedArray[mid];
        if (t === y.time) {
            return mid;
        } else if (t < y.time) {
            hi = mid;
        } else {
            lo = mid + 1;
        }
    }
    return hi;
}
var BasicScheduler = function () {
    function BasicScheduler(timer) {
        this.timer = timer;
        this._timer = null;
        this._nextArrival = 0;
        this._tasks = [];
        var self = this;
        this._runReadyTasksBound = function runReadyTasksBound() {
            self._runReadyTasks(self.now());
        };
    }
    BasicScheduler.prototype.now = function () {
        return this.timer.now();
    };
    BasicScheduler.prototype.asap = function (task) {
        return this.schedule(0, -1, task);
    };
    BasicScheduler.prototype.delay = function (delay, task) {
        return this.schedule(delay, -1, task);
    };
    BasicScheduler.prototype.periodic = function (period, task) {
        return this.schedule(0, period, task);
    };
    BasicScheduler.prototype.schedule = function (delay, period, task) {
        var now = this.now();
        var st = new ScheduledTask_1.ScheduledTask(+now + Math.max(0, delay), period, task, this);
        insertByTime(st, this._tasks);
        this._scheduleNextRun(+now);
        return st;
    };
    BasicScheduler.prototype.cancel = function (task) {
        task.active = false;
        var i = binarySearch(task.time, this._tasks);
        if (i >= 0 && i < this._tasks.length) {
            var at = array_1.findIndex(task, this._tasks[i].events);
            if (at >= 0) {
                this._tasks[i].events.splice(at, 1);
                this._reschedule();
            }
        }
    };
    BasicScheduler.prototype.cancelAll = function (f) {
        for (var i = 0; i < this._tasks.length; ++i) {
            removeAllFrom(f, this._tasks[i]);
        }
        this._reschedule();
    };
    BasicScheduler.prototype._reschedule = function () {
        if (this._tasks.length === 0) {
            this._unschedule();
        } else {
            this._scheduleNextRun(this.now());
        }
    };
    BasicScheduler.prototype._unschedule = function () {
        this.timer.clearTimer(this._timer);
        this._timer = null;
    };
    BasicScheduler.prototype._scheduleNextRun = function (now) {
        if (this._tasks.length === 0) {
            return;
        }
        var nextArrival = this._tasks[0].time;
        if (this._timer === null) {
            this._scheduleNextArrival(nextArrival, now);
        } else if (nextArrival < this._nextArrival) {
            this._unschedule();
            this._scheduleNextArrival(nextArrival, now);
        }
    };
    BasicScheduler.prototype._scheduleNextArrival = function (nextArrival, now) {
        this._nextArrival = nextArrival;
        var delay = Math.max(0, nextArrival - now);
        this._timer = this.timer.setTimer(this._runReadyTasksBound, delay);
    };
    BasicScheduler.prototype._runReadyTasks = function (now) {
        this._timer = null;
        this._tasks = this._findAndRunTasks(now);
        this._scheduleNextRun(this.now());
    };
    BasicScheduler.prototype._findAndRunTasks = function (now) {
        var tasks = this._tasks;
        var l = tasks.length;
        var i = 0;
        while (i < l && tasks[i].time <= now) {
            ++i;
        }
        this._tasks = tasks.slice(i);
        // Run all ready tasks
        for (var j = 0; j < i; ++j) {
            this._tasks = runTasks(tasks[j], this._tasks);
        }
        return this._tasks;
    };
    return BasicScheduler;
}();
exports.BasicScheduler = BasicScheduler;


},{"../util/array":28,"./ScheduledTask":13}],15:[function(require,module,exports){
"use strict";

var BasicTask = function () {
    function BasicTask(f) {
        this.f = f;
        this.active = false;
    }
    BasicTask.prototype.run = function (t) {
        if (!this.active) {
            return void 0;
        }
        var f = this.f;
        return f();
    };
    BasicTask.prototype.error = function (e) {
        throw e;
    };
    BasicTask.prototype.cancel = function () {
        this.active = false;
    };
    BasicTask.prototype.dispose = function () {};
    return BasicTask;
}();
exports.BasicTask = BasicTask;


},{}],16:[function(require,module,exports){
(function (process){
"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

var Scheduler_1 = require('./Scheduler');
var timeoutTimer_1 = require('./timeoutTimer');
var nodeTimer_1 = require('./nodeTimer');
var isNode = (typeof process === 'undefined' ? 'undefined' : _typeof(process)) === 'object' && typeof process.nextTick === 'function';
var defaultScheduler = new Scheduler_1.BasicScheduler(isNode ? nodeTimer_1.nodeTimer : timeoutTimer_1.timeoutTimer);
exports.defaultScheduler = defaultScheduler;


}).call(this,require('_process'))
},{"./Scheduler":14,"./nodeTimer":17,"./timeoutTimer":19,"_process":34}],17:[function(require,module,exports){
"use strict";

var defer_1 = require('../util/defer');
var Task_1 = require('./Task');
function isTask(t) {
    return typeof t.run === 'function' && typeof t.error === 'function' && typeof t.cancel === 'function';
}
function runAsTask(f) {
    var task = new Task_1.BasicTask(f);
    defer_1.defer(task);
    return task;
}
var nodeTimer = {
    now: Date.now,
    setTimer: function setTimer(f, dt) {
        return dt < 0 ? runAsTask(f) : setTimeout(f, dt);
    },
    clearTimer: function clearTimer(t) {
        return isTask(t) ? t.cancel() : clearTimeout(t);
    }
};
exports.nodeTimer = nodeTimer;


},{"../util/defer":29,"./Task":15}],18:[function(require,module,exports){
"use strict";

var Observer_1 = require('../sink/Observer');
var dispose_1 = require('../disposable/dispose');
var defaultScheduler_1 = require('./defaultScheduler');
function runSource(f, source, scheduler, resolve, reject) {
    var disposable = dispose_1.settable();
    var observer = new Observer_1.Observer(f, resolve, reject, disposable);
    disposable.setDisposable(source.run(observer, scheduler));
}
exports.runSource = runSource;
function withScheduler(f, source, scheduler) {
    return new Promise(function (resolve, reject) {
        runSource(f, source, scheduler, resolve, reject);
    });
}
exports.withScheduler = withScheduler;
function withDefaultScheduler(f, source) {
    return withScheduler(f, source, defaultScheduler_1.defaultScheduler);
}
exports.withDefaultScheduler = withDefaultScheduler;


},{"../disposable/dispose":10,"../sink/Observer":20,"./defaultScheduler":16}],19:[function(require,module,exports){
"use strict";

var timeoutTimer = {
    now: Date.now,
    setTimer: function setTimer(f, dt) {
        return setTimeout(f, dt);
    },
    clearTimer: function clearTimer(t) {
        return clearTimeout(t);
    }
};
exports.timeoutTimer = timeoutTimer;


},{}],20:[function(require,module,exports){
"use strict";

function disposeThen(end, error, disposable, x) {
    Promise.resolve(disposable.dispose()).then(function () {
        end(x);
    }, error);
}
var Observer = function () {
    function Observer(_event, _end, _error, disposable) {
        this._event = _event;
        this._end = _end;
        this._error = _error;
        this.disposable = disposable;
        this.active = true;
    }
    Observer.prototype.event = function (t, x) {
        if (!this.active) {
            return;
        }
        this._event(x);
    };
    Observer.prototype.end = function (t, x) {
        if (!this.active) {
            return;
        }
        disposeThen(this._end, this._error, this.disposable, x);
        this.active = false;
    };
    Observer.prototype.error = function (t, x) {
        if (!this.active) {
            return;
        }
        this.active = false;
        disposeThen(this._error, this._error, this.disposable, x);
    };
    Observer.prototype.dispose = function () {};
    return Observer;
}();
exports.Observer = Observer;


},{}],21:[function(require,module,exports){
"use strict";

var Pipe = function () {
    function Pipe(sink) {
        this.sink = sink;
        this.stopID = null;
    }
    Pipe.prototype.event = function (t, x) {
        if (this.stopID !== null) {
            clearTimeout(this.stopID);
            this.stopID = null;
        }
        this.sink.event(t, x);
    };
    Pipe.prototype.end = function (t, x) {
        var self = this;
        this.stopID = setTimeout(function () {
            self.sink.end(t, x);
            self.dispose();
        });
    };
    Pipe.prototype.error = function (t, e) {
        this.sink.error(t, e);
    };
    Pipe.prototype.dispose = function () {
        if (typeof this.sink.dispose === 'function') {
            this.sink.dispose();
        }
    };
    return Pipe;
}();
exports.Pipe = Pipe;


},{}],22:[function(require,module,exports){
"use strict";

var Stream_1 = require('../Stream');
var dispose = require('../disposable/dispose');
var PropagateTask_1 = require('../scheduler/PropagateTask');
function disposeEmpty(task) {
    return task.dispose();
}
var EmptySource = function () {
    function EmptySource() {}
    EmptySource.prototype.run = function (sink, scheduler) {
        var task = PropagateTask_1.PropagateTask.end(void 0, sink);
        scheduler.asap(task);
        return dispose.create(disposeEmpty, task);
    };
    return EmptySource;
}();
function empty() {
    return new Stream_1.Stream(new EmptySource());
}
exports.empty = empty;
var NeverSource = function () {
    function NeverSource() {}
    NeverSource.prototype.run = function () {
        return dispose.empty();
    };
    return NeverSource;
}();
function never() {
    return new Stream_1.Stream(new NeverSource());
}
exports.never = never;


},{"../Stream":1,"../disposable/dispose":10,"../scheduler/PropagateTask":12}],23:[function(require,module,exports){
"use strict";

var Stream_1 = require('../Stream');
var PropagateTask_1 = require('../scheduler/PropagateTask');
function produce(task, array, sink) {
    for (var i = 0, l = array.length; i < l && task.active; ++i) {
        sink.event(0, array[i]);
    }
    function end() {
        sink.end(0);
    }
    if (task.active) {
        end();
    }
}
function runProducer(t, array, sink) {
    produce(this, array, sink);
}
var FromProducer = function () {
    function FromProducer(arr, sink, scheduler) {
        this.arr = arr;
        this.sink = sink;
        this.scheduler = scheduler;
        this.task = new PropagateTask_1.PropagateTask(runProducer, arr, sink);
        scheduler.asap(this.task);
    }
    FromProducer.prototype.dispose = function () {
        this.task.dispose();
    };
    return FromProducer;
}();
var FromSource = function () {
    function FromSource(arr) {
        this.arr = arr;
    }
    FromSource.prototype.run = function (sink, scheduler) {
        var arr = this.arr;
        return new FromProducer(arr, sink, scheduler);
    };
    return FromSource;
}();
exports.FromSource = FromSource;
function from(x) {
    return new Stream_1.Stream(new FromSource(x));
}
exports.from = from;
function of() {
    var x = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        x[_i - 0] = arguments[_i];
    }
    return from(x);
}
exports.of = of;


},{"../Stream":1,"../scheduler/PropagateTask":12}],24:[function(require,module,exports){
"use strict";

var Stream_1 = require('../Stream');
var dispose_1 = require('../disposable/dispose');
var PropagateTask_1 = require('../scheduler/PropagateTask');
function emit(t, x, sink) {
    sink.event(t, x);
}
function cancelTask(task) {
    task.cancel();
}
var PeriodicSource = function () {
    function PeriodicSource(period, value) {
        this.period = period;
        this.value = value;
    }
    PeriodicSource.prototype.run = function (sink, scheduler) {
        var task = scheduler.periodic(this.period, new PropagateTask_1.PropagateTask(emit, this.value, sink));
        return dispose_1.create(cancelTask, task);
    };
    return PeriodicSource;
}();
exports.PeriodicSource = PeriodicSource;
function periodic(period, value) {
    return new Stream_1.Stream(new PeriodicSource(period, value));
}
exports.periodic = periodic;


},{"../Stream":1,"../disposable/dispose":10,"../scheduler/PropagateTask":12}],25:[function(require,module,exports){
"use strict";

var MulticastDisposable = function () {
    function MulticastDisposable(source, sink) {
        this.source = source;
        this.sink = sink;
    }
    MulticastDisposable.prototype.dispose = function () {
        this.source.remove(this.sink);
    };
    return MulticastDisposable;
}();
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = MulticastDisposable;


},{}],26:[function(require,module,exports){
"use strict";

var MulticastDisposable_1 = require('./MulticastDisposable');
var array_1 = require('../../util/array');
var try_1 = require('../../util/try');
function dispose(disposable) {
    return disposable.dispose();
}
var MulticastSource = function () {
    function MulticastSource(source) {
        this.source = source;
        this.sinks = [];
        this.stopID = null;
    }
    MulticastSource.prototype.run = function (sink, scheduler) {
        var n = this.add(sink);
        if (n === 1) {
            this._disposable = this.source.run(this, scheduler);
        }
        return new MulticastDisposable_1.default(this, sink);
    };
    MulticastSource.prototype._dispose = function () {
        var self = this;
        this.stopID = setTimeout(function () {
            var disposable = self._disposable;
            self._disposable = void 0;
            Promise.resolve(disposable).then(dispose);
        });
    };
    MulticastSource.prototype.add = function (sink) {
        this.sinks = array_1.append(sink, this.sinks);
        if (this.stopID !== null) {
            clearTimeout(this.stopID);
            this.stopID = null;
        }
        return this.sinks.length;
    };
    MulticastSource.prototype.remove = function (sink) {
        var _this = this;
        this.sinks = array_1.remove(array_1.findIndex(sink, this.sinks), this.sinks);
        if (this.sinks.length === 0) {
            this.stopID = setTimeout(function () {
                return _this._dispose();
            }, 20);
        }
    };
    MulticastSource.prototype.event = function (time, value) {
        var s = this.sinks;
        if (s.length === 1) {
            try_1.tryEvent(time, value, s[0]);
            return;
        }
        for (var i = 0; i < s.length; ++i) {
            try_1.tryEvent(time, value, s[i]);
        }
    };
    MulticastSource.prototype.end = function (time, value) {
        debugger;
        var s = this.sinks;
        this.stopID = setTimeout(function () {
            if (s.length === 1) {
                try_1.tryEnd(time, value, s[0]);
                return;
            }
            for (var i = 0; i < s.length; ++i) {
                try_1.tryEnd(time, value, s[i]);
            }
        });
    };
    MulticastSource.prototype.error = function (time, err) {
        var s = this.sinks;
        if (s.length === 1) {
            s[0].error(time, err);
            return;
        }
        for (var i = 0; i < s.length; ++i) {
            s[i].error(time, err);
        }
    };
    return MulticastSource;
}();
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = MulticastSource;


},{"../../util/array":28,"../../util/try":33,"./MulticastDisposable":25}],27:[function(require,module,exports){
"use strict";

var Stream_1 = require('../../Stream');
var MulticastSource_1 = require('./MulticastSource');
exports.MulticastSource = MulticastSource_1.default;
function multicast(stream) {
    var source = stream.source;
    return source instanceof MulticastSource_1.default ? stream : new Stream_1.Stream(new MulticastSource_1.default(source));
}
exports.multicast = multicast;


},{"../../Stream":1,"./MulticastSource":26}],28:[function(require,module,exports){
/** @license MIT License (c) copyright 2010-2016 original author or authors */
"use strict";
// Non-mutating array operations
// cons :: a -> [a] -> [a]
// a with x prepended

function cons(x, a) {
    var l = a.length;
    var b = new Array(l + 1);
    b[0] = x;
    for (var i = 0; i < l; ++i) {
        b[i + 1] = a[i];
    }
    return b;
}
exports.cons = cons;
// append :: a -> [a] -> [a]
// a with x appended
function append(x, a) {
    var l = a.length;
    var b = new Array(l + 1);
    for (var i = 0; i < l; ++i) {
        b[i] = a[i];
    }
    b[l] = x;
    return b;
}
exports.append = append;
// drop :: Int -> [a] -> [a]
// drop first n elements
function drop(n, a) {
    if (n < 0) {
        throw new TypeError('n must be >= 0');
    }
    var l = a.length;
    if (n === 0 || l === 0) {
        return a;
    }
    if (n >= l) {
        return [];
    }
    return unsafeDrop(n, a, l - n);
}
exports.drop = drop;
// unsafeDrop :: Int -> [a] -> Int -> [a]
// Internal helper for drop
function unsafeDrop(n, a, l) {
    var b = new Array(l);
    for (var i = 0; i < l; ++i) {
        b[i] = a[n + i];
    }
    return b;
}
// tail :: [a] -> [a]
// drop head element
function tail(a) {
    return drop(1, a);
}
exports.tail = tail;
// copy :: [a] -> [a]
// duplicate a (shallow duplication)
function copy(a) {
    var l = a.length;
    var b = new Array(l);
    for (var i = 0; i < l; ++i) {
        b[i] = a[i];
    }
    return b;
}
exports.copy = copy;
// map :: (a -> b) -> [a] -> [b]
// transform each element with f
function map(f, a) {
    var l = a.length;
    var b = new Array(l);
    for (var i = 0; i < l; ++i) {
        b[i] = f(a[i]);
    }
    return b;
}
exports.map = map;
// reduce :: (a -> b -> a) -> a -> [b] -> a
// accumulate via left-fold
function reduce(f, z, a) {
    var r = z;
    for (var i = 0, l = a.length; i < l; ++i) {
        r = f(r, a[i], i);
    }
    return r;
}
exports.reduce = reduce;
// replace :: a -> Int -> [a]
// replace element at index
function replace(x, i, a) {
    if (i < 0) {
        throw new TypeError('i must be >= 0');
    }
    var l = a.length;
    var b = new Array(l);
    for (var j = 0; j < l; ++j) {
        b[j] = i === j ? x : a[j];
    }
    return b;
}
exports.replace = replace;
// remove :: Int -> [a] -> [a]
// remove element at index
function remove(i, a) {
    if (i < 0) {
        throw new TypeError('i must be >= 0');
    }
    var l = a.length;
    if (l === 0 || i >= l) {
        return a;
    }
    if (l === 1) {
        return [];
    }
    return unsafeRemove(i, a, l - 1);
}
exports.remove = remove;
// unsafeRemove :: Int -> [a] -> Int -> [a]
// Internal helper to remove element at index
function unsafeRemove(i, a, l) {
    var b = new Array(l);
    var j;
    for (j = 0; j < i; ++j) {
        b[j] = a[j];
    }
    for (j = i; j < l; ++j) {
        b[j] = a[j + 1];
    }
    return b;
}
// removeAll :: (a -> boolean) -> [a] -> [a]
// remove all elements matching a predicate
function removeAll(f, a) {
    var l = a.length;
    var b = new Array(l);
    var j = 0;
    for (var x = void 0, i = 0; i < l; ++i) {
        x = a[i];
        if (!f(x)) {
            b[j] = x;
            ++j;
        }
    }
    b.length = j;
    return b;
}
exports.removeAll = removeAll;
// findIndex :: a -> [a] -> Int
// find index of x in a, from the left
function findIndex(x, a) {
    for (var i = 0, l = a.length; i < l; ++i) {
        if (x === a[i]) {
            return i;
        }
    }
    return -1;
}
exports.findIndex = findIndex;
// isArrayLike :: * -> boolean
// Return true iff x is array-like
function isArrayLike(x) {
    return x != null && typeof x.length === 'number' && typeof x !== 'function';
}
exports.isArrayLike = isArrayLike;


},{}],29:[function(require,module,exports){
"use strict";

function runTask(task) {
    try {
        return task.run();
    } catch (e) {
        return task.error(e);
    }
}
function defer(task) {
    return Promise.resolve(task).then(runTask);
}
exports.defer = defer;


},{}],30:[function(require,module,exports){
"use strict";

function fatalError(e) {
    setTimeout(function () {
        throw e;
    }, 0);
}
exports.fatalError = fatalError;


},{}],31:[function(require,module,exports){
"use strict";
// id :: a -> a

function id(x) {
    return x;
}
exports.id = id;
// compose :: (b -> c) -> (a -> b) -> (a -> c)
function compose(f, g) {
    return function composedFunction(x) {
        return f(g(x));
    };
}
exports.compose = compose;
// apply :: (a -> b) -> a -> b
function apply(f, x) {
    return f(x);
}
exports.apply = apply;
// curry2 :: ((a, b) -> c) -> (a -> b -> c)
function curry2(f) {
    function curried(a, b) {
        switch (arguments.length) {
            case 0:
                return curried;
            case 1:
                return function (b) {
                    return f(a, b);
                };
            default:
                return f(a, b);
        }
    }
    return curried;
}
exports.curry2 = curry2;
// curry3 :: ((a, b, c) -> d) -> (a -> b -> c -> d)
function curry3(f) {
    function curried(a, b, c) {
        switch (arguments.length) {
            case 0:
                return curried;
            case 1:
                return curry2(function (b, c) {
                    return f(a, b, c);
                });
            case 2:
                return function (c) {
                    return f(a, b, c);
                };
            default:
                return f(a, b, c);
        }
    }
    return curried;
}
exports.curry3 = curry3;


},{}],32:[function(require,module,exports){
"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

function isPromise(p) {
    return p !== null && (typeof p === 'undefined' ? 'undefined' : _typeof(p)) === 'object' && typeof p.then === 'function';
}
exports.isPromise = isPromise;


},{}],33:[function(require,module,exports){
"use strict";

function tryEvent(t, x, sink) {
    try {
        sink.event(t, x);
    } catch (e) {
        sink.error(t, e);
    }
}
exports.tryEvent = tryEvent;
function tryEnd(t, x, sink) {
    try {
        sink.end(t, x);
    } catch (e) {
        sink.error(t, e);
    }
}
exports.tryEnd = tryEnd;


},{}],34:[function(require,module,exports){
// shim for using process in browser

var process = module.exports = {};
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = setTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    clearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        setTimeout(drainQueue, 0);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };

},{}]},{},[11]);
